<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>br.unb.cic</groupId>
	<artifactId>unb-comum</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>pom</packaging>
	<description>Módulos comuns aos projetos realizados pela unb</description>
	<organization>
		<name>Universidade de Brasília</name>
		<url>http://www.cic.unb.br/</url>
	</organization>
	<inceptionYear>2016</inceptionYear>

	<modules>
		<module>unb-core</module>
		<module>unb-rest</module>
		<module>unb-jsf</module>
	</modules>

	<properties>
		<maven.compiler.source>1.8</maven.compiler.source>
		<maven.compiler.target>1.8</maven.compiler.target>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

		<versao.wildfly.bom>10.1.0.Final</versao.wildfly.bom>
		<versao.arquillian.bom>1.1.11.Final</versao.arquillian.bom>

		<versao.driver.postgresql>9.4.1211</versao.driver.postgresql>
		<versao.driver.mysql>5.1.40</versao.driver.mysql>
		<versao.driver.oracle>11.2.0</versao.driver.oracle>

		<versao.jsf>2.2.13</versao.jsf>
		<versao.servlet>3.1.0</versao.servlet>
		<versao.primefaces>6.0</versao.primefaces>
		<versao.primefaces.extensions>6.0.0</versao.primefaces.extensions>
		<versao.primefaces.all.themes>1.0.10</versao.primefaces.all.themes>
		<versao.primefaces.adamantium-theme>2.1.2</versao.primefaces.adamantium-theme>
		<versao.omnifaces>2.5.1</versao.omnifaces>
	</properties>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>br.unb.cic</groupId>
				<artifactId>unb-core</artifactId>
				<version>${project.version}</version>
			</dependency>

			<dependency>
				<groupId>br.unb.cic</groupId>
				<artifactId>unb-rest</artifactId>
				<version>${project.version}</version>
			</dependency>

			<dependency>
				<groupId>br.unb.cic</groupId>
				<artifactId>unb-jsf</artifactId>
				<version>${project.version}</version>
			</dependency>

			<dependency>
				<groupId>org.wildfly.bom</groupId>
				<artifactId>wildfly-javaee7-with-tools</artifactId>
				<scope>import</scope>
				<type>pom</type>
				<version>${versao.wildfly.bom}</version>
			</dependency>

			<dependency>
				<groupId>org.jboss.arquillian</groupId>
				<artifactId>arquillian-bom</artifactId>
				<version>${versao.arquillian.bom}</version>
				<scope>import</scope>
				<type>pom</type>
			</dependency>

			<dependency>
				<groupId>javax</groupId>
				<artifactId>javaee-api</artifactId>
				<version>7.0</version>
				<scope>provided</scope>
				<exclusions>
					<exclusion>
						<groupId>com.sun.mail</groupId>
						<artifactId>javax.mail</artifactId>
					</exclusion>
				</exclusions>
			</dependency>


			<!-- dependências para conexão com banco de dados -->
			<dependency>
				<groupId>org.postgresql</groupId>
				<artifactId>postgresql</artifactId>
				<version>${versao.driver.postgresql}</version>
				<scope>provided</scope>
			</dependency>
			<dependency>
				<groupId>mysql</groupId>
				<artifactId>mysql-connector-java</artifactId>
				<version>${versao.driver.mysql}</version>
				<scope>provided</scope>
			</dependency>
			<!-- instalar na mao por enquanto -->
			<dependency>
				<groupId>com.oracle</groupId>
				<artifactId>ojdbc6</artifactId>
				<version>${versao.driver.oracle}</version>
				<scope>provided</scope>
			</dependency>



			<dependency>
				<groupId>org.apache.commons</groupId>
				<artifactId>commons-lang3</artifactId>
				<version>3.4</version>
			</dependency>

			<dependency>
				<groupId>org.projectlombok</groupId>
				<artifactId>lombok</artifactId>
				<version>1.16.10</version>
				<scope>provided</scope>
			</dependency>

			<!-- TODO After release 2.1.7, iText moved from the MPLicense to the AGPLicense. 
				The groupId changed from com.lowagie to com.itextpdf and the artifactId from 
				itext to itextpdf https://mvnrepository.com/artifact/com.itextpdf/itextpdf -->
			<dependency>
				<groupId>com.lowagie</groupId>
				<artifactId>itext</artifactId>
				<version>4.2.2</version>
			</dependency>



			<!-- dependências usadas em tempo de compilação/execução mas providas 
				pelo wildfly -->

			<!-- TODO precisa dessa dependencia? -->
			<dependency>
				<groupId>org.jboss.resteasy</groupId>
				<artifactId>resteasy-jaxrs-testsuite</artifactId>
				<version>3.0.19.Final</version>
				<scope>provided</scope>
			</dependency>


			<!-- unb-rest -->
			<!-- swagger -->
			<dependency>
				<groupId>io.swagger</groupId>
				<artifactId>swagger-jaxrs</artifactId>
				<!-- <version>1.5.0</version> -->
				<version>1.5.10</version>
			</dependency>


			<!-- unb-jsf -->
			<!-- Configuração do JSF 
			TODO remover dependencias jsf quando estiver usando o jar eb-comum-web
			(assim que estiver disponivel em um repositorio)
			-->
			<dependency>
				<groupId>com.sun.faces</groupId>
				<artifactId>jsf-api</artifactId>
				<version>${versao.jsf}</version>
				<scope>provided</scope>
			</dependency>
			<dependency>
				<groupId>com.sun.faces</groupId>
				<artifactId>jsf-impl</artifactId>
				<version>${versao.jsf}</version>
				<scope>provided</scope>
			</dependency>
			<dependency>
				<groupId>javax.servlet</groupId>
				<artifactId>javax.servlet-api</artifactId>
				<version>${versao.servlet}</version>
				<scope>provided</scope>
			</dependency>
			<!-- Configuração do primefaces -->
			<dependency>
				<groupId>org.primefaces</groupId>
				<artifactId>primefaces</artifactId>
				<version>${versao.primefaces}</version>
			</dependency>
			<dependency>
				<groupId>org.primefaces.extensions</groupId>
				<artifactId>primefaces-extensions</artifactId>
				<version>${versao.primefaces.extensions}</version>
			</dependency>
			<dependency>
				<groupId>org.primefaces.themes</groupId>
				<artifactId>all-themes</artifactId>
				<version>${versao.primefaces.all.themes}</version>
			</dependency>
			<dependency>
				<groupId>org.primefaces.themes</groupId>
				<artifactId>adamantium-theme</artifactId>
				<version>${versao.primefaces.adamantium-theme}</version>
			</dependency>
			<!-- http://showcase.omnifaces.org/ -->
			<dependency>
				<groupId>org.omnifaces</groupId>
				<artifactId>omnifaces</artifactId>
				<version>${versao.omnifaces}</version>
			</dependency>



			<!-- dependências de TESTE -->
			<!-- TODO: usar esse ao inves do org.wildfly:wildfly-arquillian-container-managed 
				? <dependency> <groupId>org.wildfly.arquillian</groupId> <artifactId>wildfly-arquillian-container-managed</artifactId> 
				<version>2.0.0.Final</version> </dependency> -->
			<dependency>
				<groupId>org.wildfly</groupId>
				<artifactId>wildfly-arquillian-container-managed</artifactId>
				<version>8.2.1.Final</version>
				<scope>test</scope>
				<!-- exclui chamada ao jconsole. -->
				<exclusions>
					<exclusion>
						<groupId>sun.jdk</groupId>
						<artifactId>jconsole</artifactId>
					</exclusion>
				</exclusions>
			</dependency>

		</dependencies>
	</dependencyManagement>


	<dependencies>
		<dependency>
			<groupId>javax</groupId>
			<artifactId>javaee-api</artifactId>
		</dependency>
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
		</dependency>
		<dependency>
			<groupId>org.jboss.logging</groupId>
			<artifactId>jboss-logging</artifactId>
		</dependency>
		<!-- Dependências de teste -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<defaultGoal>clean install</defaultGoal>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.wildfly.plugins</groupId>
					<artifactId>wildfly-maven-plugin</artifactId>
					<version>1.1.0.Alpha11</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-war-plugin</artifactId>
					<version>3.0.0</version>
					<configuration>
						<failOnMissingWebXml>false</failOnMissingWebXml>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.5.1</version>
				<configuration>
					<source>${maven.compiler.source}</source>
					<target>${maven.compiler.target}</target>
					<encoding>${project.build.sourceEncoding}</encoding>
					<debug>false</debug>
					<optimize>true</optimize>
					<showWarnings>true</showWarnings>
					<showDeprecation>true</showDeprecation>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.19.1</version>
				<configuration>
					<trimStackTrace>false</trimStackTrace>
					<!-- Sets the VM argument line used when unit tests are run. -->
					<argLine>${surefireArgLine}</argLine>
					<!-- Skips unit tests if the value of skip.unit.tests property is true -->
					<skipTests>${skipTests}</skipTests>
					<systemPropertyVariables>
						<!-- Disponibilizando variável de ambiente para os testes (arquillian) -->
						<JBOSS_HOME>${env.JBOSS_HOME}</JBOSS_HOME>
					</systemPropertyVariables>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-eclipse-plugin</artifactId>
				<version>2.10</version>
				<configuration>
					<downloadSources>false</downloadSources>
					<downloadJavadocs>false</downloadJavadocs>
				</configuration>
			</plugin>
		</plugins>
		<extensions>
			<extension>
				<groupId>ar.com.synergian</groupId>
				<artifactId>wagon-git</artifactId>
				<version>0.2.5</version>
			</extension>
		</extensions>
	</build>

	<reporting>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>2.10.4</version>
				<configuration>
					<aggregate>true</aggregate>
					<show>private</show>
					<source>${maven.compiler.source}</source>
					<debug>false</debug>
					<links>
						<link>https://docs.oracle.com/javase/8/docs/api/</link>
						<link>https://docs.oracle.com/javaee/7/api/</link>
						<link>https://projectlombok.org/api/</link>
						<link>https://docs.jboss.org/hibernate/validator/5.2/api/</link>
						<link>https://commons.apache.org/proper/commons-lang/javadocs/api-3.4/</link>
						<link>http://junit.org/junit4/javadoc/latest/</link>
						<link>https://docs.jboss.org/resteasy/docs/3.0.9.Final/javadocs/</link>
					</links>
					<doclet>org.umlgraph.doclet.UmlGraphDoc</doclet>
					<docletArtifact>
						<groupId>org.umlgraph</groupId>
						<artifactId>umlgraph</artifactId>
						<version>5.6.6</version>
					</docletArtifact>
					<additionalparam>-inferrel -inferdep -quiet -hide java.*
						-collpackages java.util.* -qualify
						-collapsible -qualify
						-postfixpackage
						-postfixpackage
						-nodefontsize 9
						-nodefontpackagesize
						7
						-attributes -visibility -types -enumerations
						-enumconstants</additionalparam>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-plugin-plugin</artifactId>
				<version>3.5</version>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-project-info-reports-plugin</artifactId>
				<version>2.9</version>
				<configuration>
					<dependencyDetailsEnabled>false</dependencyDetailsEnabled>
					<dependencyLocationsEnabled>false</dependencyLocationsEnabled>
				</configuration>
				<reportSets>
					<reportSet>
						<reports>
							<report>index</report>
							<report>summary</report>
							<report>modules</report>
							<report>plugins</report>
							<report>plugin-management</report>
							<report>dependencies</report>
							<report>dependency-convergence</report>
							<report>dependency-management</report>
							<report>distribution-management</report>
							<report>project-team</report>
							<report>license</report>
							<report>scm</report>
							<!--<report>license</report> <report>mailing-list</report> <report>cim</report> 
								<report>issue-tracking</report> -->
						</reports>
					</reportSet>
				</reportSets>
			</plugin>
			<!-- JALOPY Plugin USO: mvn jalopy:format http://mojo.codehaus.org/jalopy-maven-plugin/ 
				http://mojo.codehaus.org/jalopy-maven-plugin/format-mojo.html <plugin> <groupId>org.codehaus.mojo</groupId> 
				<artifactId>jalopy-maven-plugin</artifactId> <version>1.0-alpha-1</version> 
				<configuration> <convention>src/jalopy/my-jalopy-conventions.xml</convention> 
				</configuration> </plugin> -->
			<!-- https://maven.apache.org/plugins/maven-changes-plugin/usage.html -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-changes-plugin</artifactId>
				<version>2.12</version>
				<reportSets>
					<reportSet>
						<reports>
							<report>changes-report</report>
						</reports>
					</reportSet>
				</reportSets>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
				<version>2.17</version>
				<reportSets>
					<reportSet>
						<reports>
							<report>checkstyle</report>
						</reports>
					</reportSet>
				</reportSets>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jxr-plugin</artifactId>
				<version>2.5</version>
				<configuration>
					<aggregate>true</aggregate>
				</configuration>
			</plugin>
			<!-- http://mojo.codehaus.org/versions-maven-plugin/usage.html -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>versions-maven-plugin</artifactId>
				<version>2.3</version>
				<reportSets>
					<reportSet>
						<reports>
							<report>dependency-updates-report</report>
							<report>plugin-updates-report</report>
							<report>property-updates-report</report>
						</reports>
					</reportSet>
				</reportSets>
			</plugin>
			<!-- Findbugs http://mojo.codehaus.org/findbugs-maven-plugin/usage.html -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>findbugs-maven-plugin</artifactId>
				<version>3.0.4</version>
				<configuration>
					<argLine>-Xmx1024m</argLine><!-- -Xmx256m -->
					<argLine>-XX:MaxPermSize=1024m</argLine> <!-- -Xmx256m -->
					<!-- Enables analysis which takes more memory but finds more bugs. If 
						you run out of memory, changes the value of the effort element to 'Low'. -->
					<effort>Max</effort>
					<!-- Reports all bugs (other values are medium and max) -->
					<threshold>Low</threshold>
					<findbugsXmlOutput>true</findbugsXmlOutput>
					<findbugsXmlOutputDirectory>target/site</findbugsXmlOutputDirectory>
					<plugins>
						<plugin>
							<groupId>com.h3xstream.findsecbugs</groupId>
							<artifactId>findsecbugs-plugin</artifactId>
							<version>LATEST</version>
						</plugin>
					</plugins>
				</configuration>
				<!-- <executions> Ensures that FindBugs inspects source code when project 
					is compiled. <execution> <id>analyze-compile</id> <phase>compile</phase> 
					<goals> <goal>check</goal> </goals> </execution> </executions> -->
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>jdepend-maven-plugin</artifactId>
				<version>2.0</version>
			</plugin>
			<!-- <plugin> <groupId>org.codehaus.mojo</groupId> <artifactId>cobertura-maven-plugin</artifactId> 
				<version>2.7</version> <configuration> <formats> <format>xml</format> <format>html</format> 
				</formats> </configuration> </plugin> -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>taglist-maven-plugin</artifactId>
				<version>2.4</version>
				<configuration>
					<encoding>${project.build.sourceEncoding}</encoding>
					<emptyComments>false</emptyComments>
					<tagListOptions>
						<tagClasses>
							<tagClass>
								<displayName>Trabalho a Fazer (TODOs e FIXMEs)</displayName>
								<tags>
									<tag>
										<matchString>todo</matchString>
										<matchType>ignoreCase</matchType>
									</tag>
									<tag>
										<matchString>FIXME</matchString>
										<matchType>exact</matchType>
									</tag>
								</tags>
							</tagClass>
							<tagClass>
								<displayName>Falta Documentar</displayName>
								<tags>
									<tag>
										<matchString>DOCUMENT_ME</matchString>
										<matchType>exact</matchType>
									</tag>
									<tag>
										<matchString>NOT_YET_DOCUMENTED</matchString>
										<matchType>exact</matchType>
									</tag>
								</tags>
							</tagClass>
						</tagClasses>
					</tagListOptions>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jxr-plugin</artifactId>
				<version>2.5</version>
				<reportSets>
					<reportSet>
						<id>aggregate</id>
						<reports>
							<report>aggregate</report>
							<report>test-aggregate</report>
						</reports>
					</reportSet>
				</reportSets>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-pmd-plugin</artifactId>
				<version>3.6</version>
				<configuration>
					<linkXref>true</linkXref>
					<minimumTokens>100</minimumTokens>
					<minimumPriority>3</minimumPriority>
					<targetJdk>${maven.compiler.source}</targetJdk>
					<sourceEncoding>${project.build.sourceEncoding}</sourceEncoding>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.owasp</groupId>
				<artifactId>dependency-check-maven</artifactId>
				<version>1.4.3</version>
				<reportSets>
					<reportSet>
						<reports>
							<report>aggregate</report>
						</reports>
					</reportSet>
				</reportSets>
			</plugin>

			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>codenarc-maven-plugin</artifactId>
				<version>0.22-1</version>
			</plugin>
			<plugin>
				<groupId>org.basepom.maven</groupId>
				<artifactId>duplicate-finder-maven-plugin</artifactId>
				<version>1.2.1</version>
				<!-- <executions> <execution> <id>default</id> <phase>verify</phase> 
					<goals> <goal>check</goal> </goals> </execution> </executions> -->
				<configuration>
					<skip>false</skip>
					<quiet>false</quiet>
					<checkCompileClasspath>true</checkCompileClasspath>
					<checkRuntimeClasspath>true</checkRuntimeClasspath>
					<checkTestClasspath>true</checkTestClasspath>
					<failBuildInCaseOfDifferentContentConflict>false</failBuildInCaseOfDifferentContentConflict>
					<failBuildInCaseOfEqualContentConflict>false</failBuildInCaseOfEqualContentConflict>
					<failBuildInCaseOfConflict>false</failBuildInCaseOfConflict>
					<printEqualFiles>false</printEqualFiles>
					<preferLocal>true</preferLocal>

					<!-- Version 1.1.1+ -->
					<includeBootClasspath>false</includeBootClasspath>
					<bootClasspathProperty>sun.boot.class.path</bootClasspathProperty>
					<!-- Version 1.1.1+ -->


					<!-- Version 1.2.0+ -->
					<includePomProjects>false</includePomProjects>
					<!-- Version 1.2.0+ -->
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-report-plugin</artifactId>
				<version>2.19.1</version>
			</plugin>
		</plugins>
	</reporting>


	<!-- http://synergian.github.io/wagon-git/usage.html -->
	<!-- TODO terminar configuracao (incluindo imagem docker) e renomear repositorios -->
	<distributionManagement>
		<repository>
			<id>unb-bitbucket-releases-repo</id>
			<name>UnB Repositório de Releases</name>
			<url>git:releases://git@bitbucket.org:promise-eb/maven-repo.git</url>
		</repository>
		<snapshotRepository>
			<id>unb-bitbucket-snapshots-repo</id>
			<name>UnB Repositório de Snapshots</name>
			<!-- <url>git:snapshots://git@bitbucket.org:promise-eb/maven-repo.git</url> -->
			<url>git:releases://git@bitbucket.org:phtcosta/maven-repo.git</url>
			<!-- https://phtcosta@bitbucket.org/phtcosta/maven-repo.git -->
		</snapshotRepository>
	</distributionManagement>

<!-- TODO rever quais repositorios sao realmente necessarios -->
	<repositories>
		<repository>
			<id>prime-repo</id>
			<name>PrimeFaces Maven Repository</name>
			<url>http://repository.primefaces.org</url>
			<layout>default</layout>
		</repository>

		<repository>
			<id>sonatype-nexus-snapshots</id>
			<name>Sonatype Nexus Snapshots</name>
			<url>http://oss.sonatype.org/content/repositories/snapshots</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>

		<repository>
			<id>unb-bitbucket-releases-repo</id>
			<name>UNB Bitbucket maven releases repo</name>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
			<url>https://api.bitbucket.org/1.0/repositories/promise-eb/maven-repo/raw/releases</url>
		</repository>
		<repository>
			<id>unb-bitbucket-snapshots-repo</id>
			<name>UNB Bitbucket maven snapshot repo</name>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
			<!-- <url>https://api.bitbucket.org/1.0/repositories/yourbitbucketusername/your-bitbucket-repo/raw/your-branch</url> -->
<!-- 			<url>https://api.bitbucket.org/1.0/repositories/phtcosta/maven-repo/raw/releases</url> -->
			<url>https://api.bitbucket.org/1.0/repositories/promise-eb/maven-repo/raw/snapshots</url>
		</repository>
	</repositories>

	<pluginRepositories>
		<!-- <pluginRepository> <id>maven.oracle.com</id> <name>oracle-maven-repo</name> 
			<url>https://maven.oracle.com</url> <layout>default</layout> <releases> <enabled>true</enabled> 
			<updatePolicy>always</updatePolicy> </releases> </pluginRepository> -->
		<pluginRepository>
			<id>synergian-repo</id>
			<url>https://raw.github.com/synergian/wagon-git/releases</url>
		</pluginRepository>

	</pluginRepositories>


	<!-- ===== DESENVOLVEDORES ===== -->
	<developers>
		<developer>
			<id>1</id>
			<name></name>
			<email></email>
			<organization></organization>
			<organizationUrl></organizationUrl>
			<roles>
				<role>Developer</role>
			</roles>
		</developer>
	</developers>
</project>
